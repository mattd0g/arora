﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestGuestbook.DB
{
    public class Rating
    {
        public virtual Guid ID { get; set; }
        public virtual Message Message { get; set; }
        public virtual byte Value { get; set; }

        public Rating() { }
        public Rating(Data.Rating source)
        {
            this.ID = source.ID;
            this.Message = new DB.Message() { ID = source.Message.ID, };
            this.Value = source.Value;
        }

        public virtual Data.Rating ToData()
        {
            return new Data.Rating()
            {
                ID = this.ID,
                Message = this.Message.ToData(false, false),
                Value = this.Value,
            };
        }
    }
}
