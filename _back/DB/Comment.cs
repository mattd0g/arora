﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestGuestbook.DB
{
    public class Comment
    {
        public virtual Guid ID { get; set; }
        public virtual DateTime Created { get; set; }
        public virtual string Author { get; set; }
        public virtual string Content { get; set; }
        public virtual Message Message { get; set; }

        public Comment()
        {

        }

        public Comment(Data.Comment source)
        {
            this.ID = source.ID;
            this.Created = source.Created;
            this.Author = source.Author;
            this.Content = source.Content;
            this.Message = new DB.Message() { ID = source.Message.ID };
        }
        
        public virtual Data.Comment ToData()
        {
            return new Data.Comment()
            {
                ID = this.ID,
                Created = this.Created,
                Author = this.Author,
                Content = this.Content,
                Message = this.Message.ToData(false, false),
            };
        }
    }
}
