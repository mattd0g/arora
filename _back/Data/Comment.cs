﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace TestGuestbook.Data
{
    [Guid("BBDA4B4D-3F8B-4FE5-BB1A-379C04805AC3")]
    [CJE.Serializable(CJE.Serializable.Politics.AllExceptExcluded)]
    public class Comment : CJE.ISerializable
    {
        public Guid ID { get; set; }
        public DateTime Created { get; set; }
        public string Author { get; set; }
        public string Content { get; set; }

        public Data.Message Message { get; set; }



    }
}
