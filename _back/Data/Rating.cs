﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace TestGuestbook.Data
{
    [Guid("948555BA-4319-4B61-B41B-F27DBCDA4728")]
    [CJE.Serializable(CJE.Serializable.Politics.AllExceptExcluded)]
    public class Rating : CJE.ISerializable
    {
        public Guid ID { get; set; }
        public Data.Message Message { get; set; }
        public byte Value { get; set; }

    }
}
