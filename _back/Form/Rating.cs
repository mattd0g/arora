﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace TestGuestbook.Form
{
    public class Rating : CJE.Form.FormData
    {
        public Rating(CJE.Form.DataRaw data) : base(data, true) { }

        [CJE.Form.Value("ID", typeof(CJE.Form.Values.GuidParser))]
        public Guid ID;

        [CJE.Form.Value("MessageID", typeof(CJE.Form.Values.GuidParser))]
        public Guid MessageID;

        [CJE.Form.Value("Value", typeof(CJE.Form.Values.IntegerParser))]
        public int Value;

        public Data.Rating ToData()
        {
            return new Data.Rating()
            {
                ID = this.ID,
                Message = new Data.Message() { ID = this.MessageID, },
                Value = (byte)this.Value,
            };
        }
    }
}
